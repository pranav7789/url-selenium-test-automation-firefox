import pytest
from selenium import webdriver
from selenium.webdriver.firefox.service import Service


def pytest_addoption(parser):
    parser.addoption("--url", action="store", default="https://google.com/")

@pytest.fixture()
def setup(pytestconfig):
    # s = Service(r"C:\Users\AAIC\Downloads\geckodriver-v0.30.0-win64\geckodriver.exe")
    options = webdriver.FirefoxOptions()
    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    driver_path =Service(r"/usr/bin/geckodriver")
    driver = webdriver.Firefox(service= driver_path,options=options)
    driver.maximize_window()
    yield {"driver": driver, "url": pytestconfig.getoption("url")}


@pytest.fixture(scope='session')
def timings(metadata):
    """
    used to calculate Analytics
    """
    Analytics = {}

    def factory(response_time):
        response_time = response_time/1000000
        throughput = 60/(1 + response_time)
        Analytics["Response_time"] = response_time
        Analytics["Throughput"] = throughput
        

    yield factory

    # add our Analytics to the json_report metadata so that we can report it out
    metadata['Analytics'] = Analytics



# @pytest.hookimpl(optionalhook=True)
# def pytest_json_modifyreport(json_report):
#     Analytics = json_report['environment']['Analytics']
